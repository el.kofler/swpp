package _20210205_DAO_mysql_notReady.persistence.models;

public class Monster {

    private String monsterTyp;
    private String attacke;

    public Monster(String monsterTyp,String attacke){
        this.monsterTyp = monsterTyp;
        this.attacke = attacke;
    }

    public void setMonsterTyp(String monsterTyp){
        this.monsterTyp = monsterTyp;
    }

    public String getMonsterTyp(){
        return monsterTyp;
    }

    public void setAttacke(String attacke){
        this.attacke = attacke;
    }

    public String getAttacke(){
        return attacke;
    }

    public String toString(){
        return getMonsterTyp() + " - " + getAttacke();
    }
    
}
