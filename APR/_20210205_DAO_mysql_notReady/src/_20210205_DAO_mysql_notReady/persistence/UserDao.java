package _20210205_DAO_mysql_notReady.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
// import java.util.Objects;
import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.models.User;

public class UserDao implements Dao<User> {
    
    public UserDao() {
        
        System.out.println("Connecting ...");
        DbController.connect();
        System.out.println("Creating Table ...");
        DbController.createUserTable();        

        this.save(new User("John", "john@domain.com"));
        this.save(new User("Susan", "susan@domain.com"));
    }
    
    @Override
    public Optional<User> get(int id) {
        // TODO: return Optional.ofNullable(users.get((int) id));
        return DbController.getUser(id);
    }
    
    @Override
    public List<User> getAll() {
        // TODO: return users;
        return DbController.getAll();
    }
    
    @Override
    public void save(User user) {
        DbController.insertUser(user.getName(), user.getEmail());
    }
    
    @Override
    public void update(User user, String[] params) {
        DbController.update(user, params);
        // user.setName(Objects.requireNonNull(
        //   params[0], "Name cannot be null"));
        // user.setEmail(Objects.requireNonNull(
        //   params[1], "Email cannot be null"));
        
        // von Tutorial korrigiert, dass User nicht doppelt in Liste
        //users.add(user);
    }
    
    @Override
    public void delete(User user) {
        // TODO: users.remove(user);
        DbController.delete(user);
    }
}

class DbController {
    static Connection conn;

    public static void insertUser(String name, String email){
        String sql = "INSERT INTO users(name,email) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Optional<User> getUser(int id){
        String sql = "SELECT * FROM Users WHERE id = ?";
        String name = "";
        String email = ""; 
        try{
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            name = rs.getString("name");
            email= rs.getString("email");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        
        return Optional.of(new User(name, email));
    }
    public static List<User> getAll(){
        String sql = "SELECT * FROM users";
        List<User> users = new ArrayList<>();
        try {
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql);
            
            // loop through the result set
            while (rs.next()) {
                users.add(new User(rs.getString("name"), rs.getString("email")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return users;
    }
    public static void update(User user, String[] params){
    String sql = "UPDATE users SET name = ? , "
                + "email = ? "
                + "WHERE name = ?";

        try {
                PreparedStatement pstmt = conn.prepareStatement(sql);

            // set the corresponding param
            pstmt.setString(1, params[0]);
            pstmt.setString(2, params[1]);
            pstmt.setString(3, user.getName());
            
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void delete(User user){
        String sql = "DELETE FROM users WHERE name = ?";

        try {
                PreparedStatement pstmt = conn.prepareStatement(sql);

            // set the corresponding param
            pstmt.setString(1, user.getName());
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    

    public static void createUserTable() {
        String sql = "CREATE TABLE IF NOT EXISTS users (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	email text\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table User exists!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connect() {
        String url = "jdbc:sqlite:./src/_20210205_DAO_mysql_notReady/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }
}
