package _20210205_DAO_mysql_notReady.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
// import java.util.Objects;
import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.models.Monster;

public class MonsterDao implements Dao<Monster>
{
    public MonsterDao() 
    {
        
    System.out.println("Connecting ...");
        DbController1.connect();
        System.out.println("Creating Table ...");
        DbController1.createMonsterTable();        

        this.save(new Monster("Feuermonster", "Feuerball"));
        this.save(new Monster("Erdmonster", "Erdbeben"));
    }
    
    @Override
    public Optional<Monster> get(int id) {
        // TODO: return Optional.ofNullable(monsters.get((int) id));
        return DbController1.getMonster(id);
    }
    
    @Override
    public List<Monster> getAll() {
        // TODO: return monsters;
        return DbController1.getAll();
    }
    
    @Override
    public void save(Monster monster) {
        DbController1.insertMonster(monster.getMonsterTyp(), monster.getAttacke());
    }
    
    @Override
    public void update(Monster monster, String[] params) {
        DbController1.update(monster, params);
        // monster.setName(Objects.requireNonNull(
        //   params[0], "Name cannot be null"));
        // monster.setEmail(Objects.requireNonNull(
        //   params[1], "Email cannot be null"));
        
        // von Tutorial korrigiert, dass Monster nicht doppelt in Liste
        //monsters.add(monster);
    }
    
    @Override
    public void delete(Monster monster) {
        // TODO: monsters.remove(monster);
        DbController1.delete(monster);
    }   
}

class DbController1 {

    static Connection conn;

    public static void insertMonster(String monsterTyp, String attacke){
        String sql = "INSERT INTO monsters(monsterTyp,attacke) VALUES(?,?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, monsterTyp);
            pstmt.setString(2, attacke);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Optional<Monster> getMonster(int id){
        String sql = "SELECT * FROM Monsters WHERE id = ?";
        String monsterTyp = "";
        String attacke = ""; 
        try{
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            monsterTyp = rs.getString("monsterTyp");
            attacke = rs.getString("attacke");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        
        return Optional.of(new Monster(monsterTyp, attacke));
    }
    public static List<Monster> getAll(){
        String sql = "SELECT * FROM monsters";
        List<Monster> monsters = new ArrayList<>();
        try {
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql);
            
            // loop through the result set
            while (rs.next()) {
                monsters.add(new Monster(rs.getString("monsterTyp"), rs.getString("attacke")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return monsters;
    }
    public static void update(Monster monster, String[] params){
    String sql = "UPDATE monsters SET monsterTyp = ? , "
                + "attacke = ? "
                + "WHERE monsterTyp = ?";

        try {
                PreparedStatement pstmt = conn.prepareStatement(sql);

            // set the corresponding param
            pstmt.setString(1, params[0]);
            pstmt.setString(2, params[1]);
            pstmt.setString(3, monster.getMonsterTyp());
            
            // update 
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void delete(Monster monster){
        String sql = "DELETE FROM monsters WHERE monsterTyp = ?";

        try {
                PreparedStatement pstmt = conn.prepareStatement(sql);

            // set the corresponding param
            pstmt.setString(1, monster.getMonsterTyp());
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    

    public static void createMonsterTable() {
        String sql = "CREATE TABLE IF NOT EXISTS monsters (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	monsterTyp text NOT NULL,\n"
                + "	attacke text\n"
                + ");";
        try {
            Statement stmt = conn.createStatement();
            stmt.execute(sql);
            System.out.println("Table Monster exists!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connect() {
        String url = "jdbc:sqlite:./src/_20210205_DAO_mysql_notReady/mydb.db";
        try {
            conn = DriverManager.getConnection(url);            
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }         
    }
}