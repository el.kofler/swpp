## Wichtige Begriffe

* **Persistence** - Daten über die Laufzeit hinaus speichern. Unterscheide Business layer von persistence Layer
* **Serialize** - Objektdaten (Datenfelder) in einen Datenstrom verwandeln, der persistiert werden kann
* **API** - Application Programming Interface
* **CRUD** - Create Read Update Delete
* **ORM** [Object-Relational Mapping](https://de.wikipedia.org/wiki/Objektrelationale_Abbildung) - Speichern von Objekten in relationaler Datenbank. Beispiel Bibliothek aus Java: **JPA**
* **Model** Begriff für das Datenmodell, sprich die Datenstruktur. Model Klassen sind mit den Entities eines ER-Modells gleichzusetzen.
* **Generic** - Eine Variable für einen Datentyp

Tutorial [Baeldung - The DAO Pattern in Java](https://www.baeldung.com/java-dao-pattern)

## weiterführend

[DAO Design Problems](http://tutorials.jenkov.com/java-persistence/dao-design-problems.html) behandelt Connection Scoping vs Instance Scope vs Thread Scope vs Transaction Scoping