package _20210205_DAO_mysql_notReady.business;

import _20210205_DAO_mysql_notReady.persistence.Dao;
import _20210205_DAO_mysql_notReady.persistence.MonsterDao;
import _20210205_DAO_mysql_notReady.persistence.UserDao;

public class DaoFactory {

    static Dao getDao(DaoEnum daoEnum) {
        
        switch(daoEnum){
            case USER:return new UserDao();
            case MONSTER:return new MonsterDao();
            default:return null;
        }

    }
}