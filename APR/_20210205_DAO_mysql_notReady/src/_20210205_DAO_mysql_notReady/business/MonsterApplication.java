package _20210205_DAO_mysql_notReady.business;

import java.util.Optional;

import _20210205_DAO_mysql_notReady.persistence.Dao;
import _20210205_DAO_mysql_notReady.persistence.MonsterDao;
import _20210205_DAO_mysql_notReady.persistence.models.Monster;

public class MonsterApplication {
    private static Dao<Monster> monsterDao;

    public static void main(String[] args) {
        monsterDao = DaoFactory.getDao(DaoEnum.MONSTER);
        
        Monster monster1 = getMonster(0);
        System.out.println("Erste Ausgabe: " + monster1);
        monsterDao.update(monster1, new String[]{"Feuermonster", "Feuerball"});
        
        Monster monster2 = getMonster(1);
        monsterDao.delete(monster2);
        monsterDao.save(new Monster("Wassermonster", "Flutwelle"));
        
        monsterDao.getAll().forEach(monster -> System.out.println(monster));
    }

    private static Monster getMonster(int id) {
        Optional<Monster> monster = monsterDao.get(id);
        
        return monster.orElse(
          new Monster("non-existing monsterTyp", "no-attack"));
    }
}
